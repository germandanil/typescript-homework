import {mapper} from "./mapper"
import { Movie,createMovieElement } from "./movie";
enum Handlers{
    getSearch,
    getPopular,
    getTopRated,
    getUpcoming,
    getAddToLocalStorage
}
interface Presets{
    loadMore:boolean,
    lastRequest:Handlers,
    lastTextSearch?:string,
    localStorageID?:string
}

async function addLocalStoageElement(url:string,currentMovies:Movie[],currentLocalStorageMovies:Movie[]):Promise<void>{
    const movie:Movie = ( await mapper(url))[0];
    currentLocalStorageMovies.push(movie);

    let movieElem:HTMLElement = (<HTMLElement>createMovieElement(movie,currentLocalStorageMovies));
    movieElem.classList.remove("col-lg-3");
    movieElem.classList.remove("col-md-4");
    (<HTMLElement>document.getElementById("favorite-movies"))?.append(movieElem);
}
async function insertContent(url:string,containerMovies:HTMLElement,currentMovies:Movie[],currentLocalStorageMovies:Movie[]):Promise<void>{
    const movies:Movie[] = await mapper(url);
    const btn:HTMLElement = <HTMLElement>document.getElementById("load-more")
    btn.style.visibility="visible";
    if(movies.length<20)
    {
       btn.style.visibility="hidden";
    }
    movies.forEach(movie=>{
        currentMovies.push(movie);
    });

    movies.forEach(movie=>{
        containerMovies.append(createMovieElement(movie,currentLocalStorageMovies));
    })
    setRandoMovie(currentMovies);
}
function setRandoMovie(currentMovies:Movie[]) {
    let randomNum:number = Math.floor(Math.random() * (currentMovies.length ));
    (<HTMLLIElement>document.getElementById("random-movie")).style.background = "url("+"https://image.tmdb.org/t/p/original/"+currentMovies[randomNum].backImgSrc+") center";
    (<HTMLLIElement>document.getElementById("random-movie")).style.objectFit = "cover";
    (<HTMLLIElement>document.getElementById("random-movie")).style.backgroundSize = "cover";
    
    ((<HTMLLIElement>document.getElementById("random-movie-name")).textContent) = <string>currentMovies[randomNum].name;
    (<HTMLParagraphElement>document.getElementById("random-movie-description")).textContent = currentMovies[randomNum].cardText;

}
function doRequest(presets:Presets,currentMovies:Movie[],currentLocalStorageMovies:Movie[]){

    let page:number=1;
    const containerMovies: HTMLElement = <HTMLElement>document.getElementById("film-container")
    if(presets.loadMore)
    {
        page += Math.floor(containerMovies.childElementCount /20);
    }
    else{
        currentMovies.splice(0,currentMovies.length);
        containerMovies.innerHTML = ""
    }
    let url:string="https://api.themoviedb.org/3";
    switch(presets.lastRequest){
        case Handlers.getSearch:
            const searchSubstringUrl:string = (<string>presets.lastTextSearch).split(" ").join("%20");
            url+= "/search/movie?api_key=5ae577f7f991b8905ba70c45ec337c2b&language=en-US&query="
                                            +searchSubstringUrl+"&page="+page+"&include_adult=false";
            insertContent(url,containerMovies,currentMovies,currentLocalStorageMovies);
            break;
        case Handlers.getPopular:
            url+= "/movie/popular?api_key=5ae577f7f991b8905ba70c45ec337c2b&language=en-US&page="+page

            insertContent(url,containerMovies,currentMovies,currentLocalStorageMovies);
            break;
        case Handlers.getUpcoming:
            url+= "/movie/upcoming?api_key=5ae577f7f991b8905ba70c45ec337c2b&language=en-US&page="+page

            insertContent(url,containerMovies,currentMovies,currentLocalStorageMovies);
            break;
        case Handlers.getTopRated:
            url+= "/movie/top_rated?api_key=5ae577f7f991b8905ba70c45ec337c2b&language=en-US&page="+page

            insertContent(url,containerMovies,currentMovies,currentLocalStorageMovies);
            break;
        case Handlers.getAddToLocalStorage:
            url+= "/movie/"+presets.localStorageID+"?api_key=5ae577f7f991b8905ba70c45ec337c2b&language=en-US"
            addLocalStoageElement(url,currentMovies,currentLocalStorageMovies);
            break;
    }

}

export {Handlers, doRequest, Presets}