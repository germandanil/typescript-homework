
import {Handlers,doRequest,Presets} from "./handlers"

type Favorite = "red"|"#ff000078";

interface Movie{
    id:string;
    posterImgSrc:string;
    backImgSrc:string;
    name:string;
    favorite:Favorite;
    cardText:string;
    date:string;
}
function createMovieElement(movie:Movie, currentLocalStorageMovies:Movie[]):HTMLElement{
    const containerWrp:HTMLElement = <HTMLElement>document.createElement("div");
    containerWrp.classList.add( "col-lg-3","col-md-4","col-12","p-2");

    const container:HTMLElement =<HTMLElement>document.createElement("div");
    container.classList.add("card","shadow-sm");

    containerWrp.append(container);

    const img:HTMLElement =  <HTMLElement>document.createElement("img");
    img.setAttribute("src","https://image.tmdb.org/t/p/original/" + movie.posterImgSrc)
    container.append(img);

    

    const heartWrp:Element = <Element>document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    heartWrp.classList.add("bi","bi-heart-fill","position-absolute","p-2");
    heartWrp.setAttribute("xmlns","http://www.w3.org/2000/svg");
    heartWrp.setAttribute("stroke","red");
    heartWrp.setAttribute("fill",movie.favorite);
    heartWrp.setAttribute("width","50");
    heartWrp.setAttribute("height","50");
    heartWrp.setAttribute("viewBox","0 -2 18 22");

    container.append(heartWrp);


    const heart:Element =getHeartWithEventListener(heartWrp,currentLocalStorageMovies,movie);

    heartWrp.append(heart)

    const cardBody:HTMLElement  =  <HTMLElement>document.createElement("div");
    cardBody.className = "card-body";
    
    container.append(cardBody);

    const cardText:HTMLElement =  <HTMLElement>document.createElement("p");
    cardText.classList.add("card-text", "truncate");
    cardText.innerHTML = movie.cardText;

    cardBody.append(cardText);
    
    const cardDateWrp:HTMLElement =  <HTMLElement>document.createElement("div");
    cardDateWrp.classList.add("d-flex","justify-content-between","align-items-center");
    
    cardBody.append(cardDateWrp);

    const cardDate:HTMLElement =  <HTMLElement>document.createElement("small");
    cardDate.className = "text-muted";
    cardDate.innerHTML = movie.date;

    cardDateWrp.append(cardDate);
    return containerWrp;
}
function getHeartWithEventListener(heartWrp:Element,currentLocalStorageMovies:Movie[],movie:Movie):Element {
    const heart:Element =  <Element>document.createElementNS('http://www.w3.org/2000/svg',"path");
    heart.setAttribute("fill-rule","evenodd");
    heart.setAttribute("d","M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z")
    heart.setAttribute("cursor","pointer");
    heart.addEventListener("click", ()=>{
        const presets:Presets=  {
            loadMore:true,
            lastRequest:Handlers.getAddToLocalStorage,
            localStorageID:movie.id        
        }
        if(heartWrp.getAttribute("fill")  ==="#ff000078" ){
            heartWrp.setAttribute("fill","red")
            localStorage.setItem(movie.id,movie.id);
            doRequest(presets,[],currentLocalStorageMovies)
        }
        else{
            let num:number = 0;
            currentLocalStorageMovies.forEach(m =>{
                if(movie.id==m.id){
                    const favoriteElem:HTMLElement = <HTMLElement>document.querySelectorAll("#favorite-movies .col-12.p-2")[num];
                    document.querySelectorAll("#film-container .card.shadow-sm").forEach(
                        elem=>{
                            if((<HTMLElement>elem.childNodes[0]).getAttribute("src") == (<HTMLElement>favoriteElem.childNodes[0].childNodes[0]).getAttribute("src"))
                                (<HTMLElement>elem.childNodes[1]).setAttribute("fill","#ff000078");
                        })
                    favoriteElem.remove()
                    currentLocalStorageMovies.splice(num,1);
                }
                num++;
            })

            localStorage.removeItem(<string>presets.localStorageID)
        }
        return heart;
    })
    return heart;
}

export {Movie,createMovieElement}