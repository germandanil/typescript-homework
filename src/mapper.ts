import {callApi}from "./apiHelper"
import { isIncludeKey } from "./helperLocalSotarge";
import {Movie} from "./movie"
export  async function mapper(url:string):Promise<Movie[]>{
            let call = await callApi(url);
            const response =call.hasOwnProperty("title")? {results: [call]}:call;
            const results = response.results;
            const movies:Movie[]=[];

            if(response.hasOwnProperty("error"))
                return movies;
            results.forEach(element => {
                const movie:Movie = { 
                    id:element.id,
                    posterImgSrc:element.poster_path,
                    backImgSrc:element.backdrop_path,
                    name:element.original_title,
                    favorite: isIncludeKey(element.id)?"red":"#ff000078",
                    cardText:element.overview,
                    date:element.release_date}
                    movies.push(movie);
            });
        
            return movies;
        }