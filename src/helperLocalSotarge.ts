function allStorage(): string[]{
    const values:string[] = [],keys = Object.keys(localStorage);
    let i:number= <number>keys.length;
    while ( i-- ) {
        values.push( <string>localStorage.getItem(keys[i]) );
    }
    return values;
}

function isIncludeKey(key:string):boolean{
    return localStorage.getItem(key)!==null
}

export {allStorage,isIncludeKey}