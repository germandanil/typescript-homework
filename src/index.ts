// import { main } from "@popperjs/core";
import {Handlers,doRequest,Presets} from "./handlers"
import { Movie } from "./movie";
import { allStorage} from "./helperLocalSotarge";



export async function render(): Promise<void> {
    const presets:Presets=  {
        loadMore:false,
        lastRequest:Handlers.getPopular          
    }
    const currentMovies:Movie[] = [];
    const currentStorageMoviesID:string[]=  allStorage();
    const currentStorageMovies:Movie[] = [];
    try{
        doRequest(presets,currentMovies,currentStorageMovies);

        presets.loadMore = true;
        presets.lastRequest=Handlers.getAddToLocalStorage,
        currentStorageMoviesID.forEach(id=>{
            presets.localStorageID = id
            doRequest(presets,currentMovies,currentStorageMovies);
        })
        
        presets.lastRequest = Handlers.getPopular;
        document.getElementById("load-more")?.addEventListener("click",()=>{
            presets.loadMore = true;
            doRequest(presets,currentMovies,currentStorageMovies);
        });
        document.getElementById("top_rated")?.addEventListener("click",()=>{
            presets.lastRequest = Handlers.getTopRated;
            presets.loadMore = false;
            doRequest(presets,currentMovies,currentStorageMovies);
        });
        document.getElementById("upcoming")?.addEventListener("click",()=>{
            presets.lastRequest = Handlers.getUpcoming;
            presets.loadMore = false;
            doRequest(presets,currentMovies,currentStorageMovies);
        });
        document.getElementById("popular")?.addEventListener("click",()=>{
            presets.lastRequest = Handlers.getPopular;
            presets.loadMore = false;
            doRequest(presets,currentMovies,currentStorageMovies);
        });
        document.getElementById("submit")?.addEventListener("click",()=>{
            const searchBar:HTMLInputElement = <HTMLInputElement>document.getElementById("search");
            const searchText:string = searchBar.value;

            presets.lastRequest = Handlers.getSearch;
            presets.lastTextSearch = searchText;
            presets.loadMore = false;

            if(searchText!=="")
                doRequest(presets,currentMovies,currentStorageMovies);
        });

    }
    catch(error){
        console.warn(error)
    }
}
